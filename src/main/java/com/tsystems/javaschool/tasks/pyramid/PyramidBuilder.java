package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        for (Integer curr : inputNumbers)
            if (curr == null) throw new CannotBuildPyramidException();
        if (inputNumbers.size() >= 256) throw new CannotBuildPyramidException();
        long length = inputNumbers.size();
        int arithmLvl = 0, arithmSum = 0;

        while (length > arithmSum) {
            arithmLvl++;
            arithmSum += arithmLvl;
        }
        if (length != arithmSum) throw new CannotBuildPyramidException();
        Collections.sort(inputNumbers);

        int numOfRows = arithmLvl;
        int numOfColumns = arithmLvl * 2 - 1;
        int[][] expected = new int[numOfRows][numOfColumns];
        int numOfWriten = 0;
        int currNumOfWriten;

        for (int i = 0; i < numOfRows; i++) {
            currNumOfWriten = 0;
            while (currNumOfWriten < i + 1) {
                expected[i][arithmLvl - i - 1 + currNumOfWriten * 2] = inputNumbers.get(numOfWriten);
                currNumOfWriten++;
                numOfWriten++;
            }
        }

        return expected;
    }


}
