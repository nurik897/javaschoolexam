package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.util.ArrayDeque;
import java.util.Deque;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    private static final String NUMFORMAT = "#0.####";
    private static final String[] OPERATORS = {"+", "-", "*", "/", "(", ")"};
    private Deque<Double> deqNumber;
    private Deque<Character> deqOperators;

    public boolean checkBrackets(String s) {
        int countBrackets = 0;
        char line[] = s.toCharArray();
        for (char elem : line) {
            if (elem == '(') countBrackets++;
            if (elem == ')') countBrackets--;
        }
        return countBrackets == 0;
    }

    public String conversionString(String s) {
        for (String tmp : OPERATORS) s = s.replace(tmp, " " + tmp + " ");
        return s;
    }

    public boolean isOperator(String s) {
        return s.equals("+") || s.equals("-") || s.equals("/") || s.equals("*");
    }

    public int priority(char operator) {
        switch (operator) {
            case '*':
            case '/':
                return 1;
            case '+':
            case '-':
                return 0;
            default:
                return -1;
        }
    }

    public void fillStack(String s[]) {
        for (String s1 : s) {
            if (s1.equals("(")) deqOperators.add(s1.charAt(0));
            else if (s1.equals(")")) {
                while (deqOperators.getLast() != '(') calculate(deqNumber, deqOperators.removeLast());
                deqOperators.removeLast();
            } else if (isOperator(s1)) {
                while (!deqOperators.isEmpty() &&
                        priority(deqOperators.getLast()) >= priority(s1.charAt(0))) {
                    calculate(deqNumber, deqOperators.removeLast());
                }
                deqOperators.add(s1.charAt(0));
            } else if (!s1.equals("")) {
                deqNumber.add(Double.parseDouble(s1));
            }
        }
    }

    public void calculate(Deque<Double> st, char operator) {
        double first = st.removeLast();
        double second = st.removeLast();
        switch (operator) {
            case '+':
                st.add(second + first);
                break;
            case '-':
                st.add(second - first);
                break;
            case '*':
                st.add(second * first);
                break;
            case '/':
                if (first == 0) throw new ArithmeticException();
                else st.add(second / first);
                break;
            default:
                break;
        }
    }

    public String evaluate(String statement) {
        if (statement != null && checkBrackets(statement)) {
            statement = conversionString(statement.replace(" ", ""));
            String arrStrings[] = statement.split(" ");
            deqNumber = new ArrayDeque<>();
            deqOperators = new ArrayDeque<>();
            try {
                fillStack(arrStrings);
                while (!deqOperators.isEmpty()) {
                    calculate(deqNumber, deqOperators.removeLast());
                }
                DecimalFormat myFormatter = new DecimalFormat(NUMFORMAT);
                statement = myFormatter.format(deqNumber.getFirst());
                return statement.replace(",", ".");
            } catch (RuntimeException ex) {
                return null;
            }
        } else return null;
    }
}
